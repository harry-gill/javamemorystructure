package com.example;

public class Person {

	String fName = "John";
	String lName = new String("Doe");
	int age = 25;
	Car car = new Car();

	public void drive() {
		// Logic for this person to drive
	}

}
